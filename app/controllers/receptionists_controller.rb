class ReceptionistsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_receptionist_role
  
    def index
      @patients = Patient.all
    end
  
    def new
      @patient = Patient.new
    end
  
    def create
      @patient = Patient.new(patient_params)
      if @patient.save
        redirect_to receptionists_path, notice: 'Patient was successfully created.'
      else
        render :new
      end
    end
  
    def edit
      @patient = Patient.find(params[:id])
    end
  
    def update
      @patient = Patient.find(params[:id])
      if @patient.update(patient_params)
        redirect_to receptionists_path, notice: 'Patient was successfully updated.'
      else
        render :edit
      end
    end
  
    def show
      @patient = Patient.find(params[:id])
    end
  
    def destroy
      @patient = Patient.find(params[:id])
      @patient.destroy
      redirect_to receptionists_path, notice: 'Patient was successfully destroyed.'
    end
  
    private
  
    def patient_params
      params.require(:patient).permit(:name, :age)
    end
  
    def check_receptionist_role
      redirect_to root_path, alert: 'Access denied. You are not a receptionist.' unless current_user.role == 'receptionist'
    end
  end
  
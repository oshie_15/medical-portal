class DoctorsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_doctor_role
  
    def index
      @patients = Patient.all
      @patients_by_day = Patient.group_by_day(:created_at).count
    end
  
    def show
      @patient = Patient.find(params[:id])
    end
  
    private
  
    def check_doctor_role
      redirect_to root_path, alert: 'Access denied. You are not a doctor.' unless current_user.role == 'doctor'
    end
  end
  